package com.techelevator.tenmo.dao;

import com.techelevator.tenmo.model.Account;
import com.techelevator.tenmo.model.Transfer;

import java.util.List;

public interface TransferDAO {

    List<Transfer> getListOfTransfers(int userId);

    Transfer createTransfer(Transfer transfer);

    Transfer createPendingTransfer(Transfer transfer);

    Transfer updatePendingTransferToApproved(Transfer transfer);

    Transfer updatePendingTransferToRejected(Transfer transfer);

    Transfer retrieveTransferDetailsByTransferId(Transfer transferId);
}
