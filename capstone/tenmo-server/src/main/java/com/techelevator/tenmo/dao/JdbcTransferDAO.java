package com.techelevator.tenmo.dao;

import com.techelevator.tenmo.model.Transfer;
import com.techelevator.tenmo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class JdbcTransferDAO implements TransferDAO{

    @Autowired
    private UserDAO userDAO;

    private static List<Transfer> transfers = new ArrayList<>();

    private JdbcTemplate jdbcTemplate;

    public JdbcTransferDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Transfer> getListOfTransfers(int userId) {
        List<Transfer> transfers = new ArrayList<>();
        String sql = "SELECT transfers.* FROM transfers " +
                "JOIN accounts ON accounts.account_id = transfers.account_from " +
                "OR accounts.account_id = transfers.account_to " +
                "WHERE accounts.user_id = ?";
        SqlRowSet results = jdbcTemplate.queryForRowSet(sql, userId);
        while(results.next()) {
            Transfer transfer = mapRowToTransfer(results);
            transfer.setAccountFromUsername(userDAO.findUsernameByAccountId(transfer.getAccountFromId()));
            transfer.setAccountToUsername(userDAO.findUsernameByAccountId(transfer.getAccountToId()));
            transfers.add(transfer);
        }
        return transfers;
    }

    @Override
    public Transfer createTransfer(Transfer transfer) {
        int nextTransferId = getNextTransferId();

        String transferSQL = "INSERT INTO transfers(transfer_id, transfer_type_id, transfer_status_id, account_from, account_to, amount) " +
                "VALUES(?, ?, ?, ?, ?, ?)";


        jdbcTemplate.update(transferSQL,
                nextTransferId,
                transfer.getTransferTypeId(),
                transfer.getTransferStatusId(),
                transfer.getAccountFromId(),
                transfer.getAccountToId(),
                transfer.getTransferAmount()
        );

        String accountFromSQL = "UPDATE accounts " +
                "SET balance = balance - ? " +
                "WHERE account_id = ?";


        jdbcTemplate.update(accountFromSQL,
                transfer.getTransferAmount(),
                transfer.getAccountFromId()
        );

        String accountToSQL = "UPDATE accounts " +
                "SET balance = balance + ? " +
                "WHERE account_id = ?";


        jdbcTemplate.update(accountToSQL,
                transfer.getTransferAmount(),
                transfer.getAccountToId()
        );

        return transfer;
    }

    @Override
    public Transfer createPendingTransfer(Transfer pendingTransfer) {
        int nextTransferId = getNextTransferId();

        String transferSQL = "INSERT INTO transfers(transfer_id, transfer_type_id, transfer_status_id, account_from, account_to, amount) " +
                "VALUES(?, ?, ?, ?, ?, ?)";


        jdbcTemplate.update(transferSQL,
                nextTransferId,
                pendingTransfer.getTransferTypeId(),
                pendingTransfer.getTransferStatusId(),
                pendingTransfer.getAccountFromId(),
                pendingTransfer.getAccountToId(),
                pendingTransfer.getTransferAmount()
        );

        return pendingTransfer;
    }

    @Override
    public Transfer updatePendingTransferToApproved(Transfer pendingTransfer) {
        String transferSQL = "UPDATE transfers " +
                "SET transfer_status_id = ? " +
                "WHERE transfer_id = ?";

        jdbcTemplate.update(transferSQL,
                pendingTransfer.getTransferStatusId(),
                pendingTransfer.getTransferId()
        );

        String accountFromSQL = "UPDATE accounts " +
                "SET balance = balance - ? " +
                "WHERE account_id = ?";


        jdbcTemplate.update(accountFromSQL,
                pendingTransfer.getTransferAmount(),
                pendingTransfer.getAccountFromId()
        );

        String accountToSQL = "UPDATE accounts " +
                "SET balance = balance + ? " +
                "WHERE account_id = ?";


        jdbcTemplate.update(accountToSQL,
                pendingTransfer.getTransferAmount(),
                pendingTransfer.getAccountToId()
        );

        return pendingTransfer;
    }

    @Override
    public Transfer updatePendingTransferToRejected(Transfer pendingTransfer) {
        String transferSQL = "UPDATE transfers " +
                "SET transfer_status_id = ? " +
                "WHERE transfer_id = ?";

        jdbcTemplate.update(transferSQL,
                pendingTransfer.getTransferStatusId(),
                pendingTransfer.getTransferId()
        );

        return pendingTransfer;
    }

    @Override
    public Transfer retrieveTransferDetailsByTransferId(Transfer transferId) {
        return null;
    }

    private Transfer mapRowToTransfer(SqlRowSet rs) {
        Transfer transfer = new Transfer();
        transfer.setTransferId(rs.getInt("transfer_id"));
        transfer.setTransferTypeId(rs.getInt("transfer_type_id"));
        transfer.setTransferStatusId(rs.getInt("transfer_status_id"));
        transfer.setAccountFromId(rs.getInt("account_from"));
        transfer.setAccountToId(rs.getInt("account_to"));
        transfer.setTransferAmount(rs.getBigDecimal("amount"));
        return transfer;
    }

    private int getNextTransferId() {
        SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_transfer_id'::regclass)");
        if(nextIdResult.next()) {
            return nextIdResult.getInt(1);
        } else {
            throw new RuntimeException("Something went wrong while getting an id for the new address");
        }
    }
}
