package com.techelevator.tenmo.dao;

import com.techelevator.tenmo.model.Account;

import javax.security.auth.login.AccountNotFoundException;

public interface AccountDAO {

    Account retrieveAccountBalanceByUserId(int userId) throws AccountNotFoundException;

}
