package com.techelevator.tenmo.controller;

import com.techelevator.tenmo.dao.AccountDAO;
import com.techelevator.tenmo.dao.UserDAO;
import com.techelevator.tenmo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserDAO dao;

    @RequestMapping(path = "/users", method = RequestMethod.GET)
    public List<User> findAll() {
        return dao.findAll();
    }


    //TODO use principal
 //   @RequestMapping(path = "/users", method = RequestMethod.GET)
   // public int findIdByUsername(Principal principal) {
     //   String user = principal.getName();
       // return dao.findIdByUsername(user);
    //}

}
