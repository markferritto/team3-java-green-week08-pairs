package com.techelevator.tenmo.controller;

import com.techelevator.tenmo.dao.AccountDAO;
import com.techelevator.tenmo.dao.TransferDAO;
import com.techelevator.tenmo.dao.UserDAO;
import com.techelevator.tenmo.model.Account;
import com.techelevator.tenmo.model.Transfer;
import com.techelevator.tenmo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.AccountNotFoundException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@PreAuthorize("isAuthenticated()")
public class TransferController {

    @Autowired
    private AccountDAO dao;
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private TransferDAO transferDAO;

    @RequestMapping(path = "/transfers", method = RequestMethod.GET)
    public List<Transfer> listAllTransfers(Principal principal) {
        String transfer = principal.getName();
        return transferDAO.getListOfTransfers(userDAO.findIdByUsername(transfer));
    }

    @RequestMapping(path = "/transfers", method = RequestMethod.PUT)
    public Transfer updateTransfer(@RequestBody Transfer transfer) {

        if (transfer.getTransferStatusId() == 2) {
            return transferDAO.updatePendingTransferToApproved(transfer);
        } else {
            return transferDAO.updatePendingTransferToRejected(transfer);
        }
    }


    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(path = "/transfers", method = RequestMethod.POST)
    public Transfer createTransfer(@Valid @RequestBody Transfer transfer, Principal principal) throws AccountNotFoundException {

        if (transfer.getTransferTypeId() == 1) {

            String user = principal.getName();
            //to get fromAccount accountId
            int fromUserId = transfer.getAccountFromId();
            Account fromAccount = dao.retrieveAccountBalanceByUserId(fromUserId);
            int fromAccountId = fromAccount.getAccountId();
            transfer.setAccountFromId(fromAccountId);

            //to get toAccount accountId
            int toUserId = userDAO.findIdByUsername(user);
            Account toAccount = dao.retrieveAccountBalanceByUserId(toUserId);
            int toAccountId = toAccount.getAccountId();
            transfer.setAccountToId(toAccountId);

            return transferDAO.createPendingTransfer(transfer);

        } else {

            String user = principal.getName();
            //to get fromAccount accountId
            int fromUserId = userDAO.findIdByUsername(user);
            Account fromAccount = dao.retrieveAccountBalanceByUserId(fromUserId);
            int fromAccountId = fromAccount.getAccountId();
            transfer.setAccountFromId(fromAccountId);

            //to get toAccount accountId
            int toUserId = transfer.getAccountToId();
            Account toAccount = dao.retrieveAccountBalanceByUserId(toUserId);
            int toAccountId = toAccount.getAccountId();
            transfer.setAccountToId(toAccountId);
            return transferDAO.createTransfer(transfer);
        }
    }
}
