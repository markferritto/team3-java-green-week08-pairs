package com.techelevator.tenmo.controller;

import com.techelevator.tenmo.dao.AccountDAO;
import com.techelevator.tenmo.dao.UserDAO;
import com.techelevator.tenmo.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.AccountNotFoundException;
import java.security.Principal;
import java.util.List;

@RestController
@PreAuthorize("isAuthenticated()")
public class AccountController {

    @Autowired
    private AccountDAO dao;
    @Autowired
    private UserDAO userDAO;

    @RequestMapping(path = "/accounts", method  = RequestMethod.GET)
    public Account retrieveAccountBalanceByUserId(Principal principal) throws AccountNotFoundException {
        String user = principal.getName();
        return dao.retrieveAccountBalanceByUserId(userDAO.findIdByUsername(user));
    }
}
