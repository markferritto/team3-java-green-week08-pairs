package com.techelevator.tenmo.services;

import com.techelevator.tenmo.models.Transfer;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

public class TransferService {

    private final String BASE_SERVICE_URL;
    public String AUTH_TOKEN = "";
    private RestTemplate restTemplate = new RestTemplate();

    public TransferService(String baseUrl) {
        this.BASE_SERVICE_URL = baseUrl;
    }

    public Transfer[] retrieveTransfersByUserId() throws TransferServiceException {
        Transfer[] transfers = null;

        try {
            transfers = restTemplate.exchange(BASE_SERVICE_URL + "transfers", HttpMethod.GET, makeAuthEntity(), Transfer[].class).getBody();
        } catch (RestClientResponseException ex) {
            if (ex.getRawStatusCode() == 400) {
                return null;
            }
            throw new TransferServiceException(ex.getRawStatusCode() + " : " + ex.getResponseBodyAsString());
        }
        return transfers;
    }

    public void createTransfer(Transfer transfer) throws TransferServiceException {
        try {
            restTemplate.exchange(BASE_SERVICE_URL + "transfers", HttpMethod.POST, makeTransferEntity(transfer), Transfer.class).getBody();
        } catch (RestClientResponseException ex) {
            throw new TransferServiceException(ex.getRawStatusCode() + " : " + ex.getResponseBodyAsString());
        }
    }

    public void updateTransfer(Transfer transfer) throws TransferServiceException {
        try {
        restTemplate.exchange(BASE_SERVICE_URL + "transfers", HttpMethod.PUT, makeTransferEntity(transfer), Transfer.class).getBody();
        } catch (RestClientResponseException ex) {
            throw new TransferServiceException(ex.getRawStatusCode() + " : " + ex.getResponseBodyAsString());
        }
    }

    private HttpEntity<Transfer> makeTransferEntity(Transfer transfer) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(AUTH_TOKEN);
        HttpEntity<Transfer> entity = new HttpEntity<>(transfer, headers);
        return entity;
    }

    // MAKE AN AUTH ENTITY ONLY
    private HttpEntity makeAuthEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(AUTH_TOKEN);
        HttpEntity entity = new HttpEntity<>(headers);
        return entity;
    }

}
