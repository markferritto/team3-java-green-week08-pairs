package com.techelevator.tenmo;

import com.techelevator.tenmo.models.*;
import com.techelevator.tenmo.services.*;
import com.techelevator.view.ConsoleService;
import io.cucumber.java.bs.A;

import javax.security.auth.login.AccountNotFoundException;
import java.math.BigDecimal;
import java.sql.SQLOutput;

public class App {

    private static final String API_BASE_URL = "http://localhost:8080/";

    private static final String MENU_OPTION_EXIT = "Exit";
    private static final String LOGIN_MENU_OPTION_REGISTER = "Register";
    private static final String LOGIN_MENU_OPTION_LOGIN = "Login";
    private static final String[] LOGIN_MENU_OPTIONS = {LOGIN_MENU_OPTION_REGISTER, LOGIN_MENU_OPTION_LOGIN, MENU_OPTION_EXIT};
    private static final String MAIN_MENU_OPTION_VIEW_BALANCE = "View your current balance";
    private static final String MAIN_MENU_OPTION_SEND_BUCKS = "Send TE bucks";
    private static final String MAIN_MENU_OPTION_VIEW_PAST_TRANSFERS = "View your past transfers";
    private static final String MAIN_MENU_OPTION_REQUEST_BUCKS = "Request TE bucks";
    private static final String MAIN_MENU_OPTION_VIEW_PENDING_REQUESTS = "View your pending requests";
    private static final String MAIN_MENU_OPTION_LOGIN = "Login as different user";
    private static final String[] MAIN_MENU_OPTIONS = {MAIN_MENU_OPTION_VIEW_BALANCE, MAIN_MENU_OPTION_SEND_BUCKS, MAIN_MENU_OPTION_VIEW_PAST_TRANSFERS, MAIN_MENU_OPTION_REQUEST_BUCKS, MAIN_MENU_OPTION_VIEW_PENDING_REQUESTS, MAIN_MENU_OPTION_LOGIN, MENU_OPTION_EXIT};

    private AuthenticatedUser currentUser;
    private ConsoleService console;
    private AuthenticationService authenticationService;
    private AccountService accountService;
    private UserService userService;
    private TransferService transferService;

    public static void main(String[] args) throws TransferServiceException {
        App app = new App(new ConsoleService(System.in, System.out), new AuthenticationService(API_BASE_URL), new AccountService(API_BASE_URL), new UserService(API_BASE_URL), new TransferService(API_BASE_URL));
        app.run();
    }

    public App(ConsoleService console, AuthenticationService authenticationService, AccountService accountService, UserService userService, TransferService transferService) {
        this.console = console;
        this.authenticationService = authenticationService;
        this.accountService = accountService;
        this.userService = userService;
        this.transferService = transferService;
    }

    public void run() throws TransferServiceException {
        System.out.println("*********************");
        System.out.println("* Welcome to TEnmo! *");
        System.out.println("*********************");

        registerAndLogin();
        mainMenu();
    }

    private void mainMenu() throws TransferServiceException {
        while (true) {
            String choice = (String) console.getChoiceFromOptions(MAIN_MENU_OPTIONS);
            if (MAIN_MENU_OPTION_VIEW_BALANCE.equals(choice)) {
                viewCurrentBalance();
            } else if (MAIN_MENU_OPTION_VIEW_PAST_TRANSFERS.equals(choice)) {
                viewTransferHistory();
            } else if (MAIN_MENU_OPTION_VIEW_PENDING_REQUESTS.equals(choice)) {
                viewPendingRequests();
            } else if (MAIN_MENU_OPTION_SEND_BUCKS.equals(choice)) {
                sendBucks();
            } else if (MAIN_MENU_OPTION_REQUEST_BUCKS.equals(choice)) {
                requestBucks();
            } else if (MAIN_MENU_OPTION_LOGIN.equals(choice)) {
                login();
            } else {
                // the only other option on the main menu is to exit
                exitProgram();
            }
        }
    }

    private void viewCurrentBalance() {
        Account account = accountService.retrieveAccount();
        console.printCurrentBalance(account.getBalance());
    }

    private void viewTransferHistory() throws TransferServiceException {
        Transfer[] transfers = transferService.retrieveTransfersByUserId();
        Account account = accountService.retrieveAccount();
        int accountId = account.getAccountId();
        int userResponse = console.printListOfTransfers(transfers, accountId);
        handleIndividualTransfer(transfers, userResponse);
    }

    private void handleIndividualTransfer(Transfer[] transfers, int transferId) throws TransferServiceException {
        if (transferId == 0) {
            mainMenu();
        }
        for (Transfer transfer : transfers) {
            if (transfer.getTransferId() == transferId) {
                console.printTransferDetails(transfer);
            }
        }
    }

    private void viewPendingRequests() throws TransferServiceException {

        Transfer[] transfers = transferService.retrieveTransfersByUserId();
        Account account = accountService.retrieveAccount();
        int accountId = account.getAccountId();
        int userResponse = console.printListOfPendingTransfers(transfers, accountId);
        handlePendingTransfer(transfers, userResponse);
    }

    private void handlePendingTransfer(Transfer[] transfers, int transferId) throws TransferServiceException {
        if (transferId == 0) {
            mainMenu();
        }
        for (Transfer transfer : transfers) {
            if (transfer.getTransferId() == transferId) {
                int pendingChoice = console.printPendingTransferOptions(transfer);
                if (pendingChoice == 0) {
                    mainMenu();
                } else if (pendingChoice == 1) {
                    Account account = accountService.retrieveAccount();
                    if (transfer.getTransferAmount().compareTo(account.getBalance()) == 1) {
                        console.printInsufficientFunds();
                    } else {
                        transfer.setTransferStatusId(2);
                        transferService.updateTransfer(transfer);
                    }
                } else if (pendingChoice == 2) {
                    transfer.setTransferStatusId(3);
                    transferService.updateTransfer(transfer);
                }
            }
        }
    }

    private void sendBucks() throws TransferServiceException {
        User[] users = userService.retrieveAllUsers();
        Transfer transfer = console.printListOfUsersToSendMoney(users, currentUser.getUser().getUsername());
        if (transfer.getAccountToId() == 0) {
            mainMenu();
        }
        if (users.length + 1000 >= transfer.getAccountToId()) {
            Account account = accountService.retrieveAccount();
            if (transfer.getTransferAmount().compareTo(account.getBalance()) == 1) {
                console.printInsufficientFunds();
            } else {
                transferService.createTransfer(transfer);
            }
        } else {
            console.printNotValidOption();
            sendBucks();
        }
    }


    private void requestBucks() throws TransferServiceException {
        User[] users = userService.retrieveAllUsers();
        Transfer transfer = console.printListOfUsersToRequestMoney(users, currentUser.getUser().getUsername());
        if (transfer.getAccountFromId() == 0) {
            mainMenu();
        }
        if (users.length + 1000 >= transfer.getAccountFromId()) {
            transferService.createTransfer(transfer);
        } else {
            console.printNotValidOption();
            requestBucks();
        }
    }


    private void exitProgram() {
        System.exit(0);
    }

    private void registerAndLogin() {
        while (!isAuthenticated()) {
            String choice = (String) console.getChoiceFromOptions(LOGIN_MENU_OPTIONS);
            if (LOGIN_MENU_OPTION_LOGIN.equals(choice)) {
                login();
            } else if (LOGIN_MENU_OPTION_REGISTER.equals(choice)) {
                register();
            } else {
                // the only other option on the login menu is to exit
                exitProgram();
            }
        }
    }

    private boolean isAuthenticated() {
        return currentUser != null;
    }

    private void register() {
        System.out.println("Please register a new user account");
        boolean isRegistered = false;
        while (!isRegistered) //will keep looping until user is registered
        {
            UserCredentials credentials = collectUserCredentials();
            try {
                authenticationService.register(credentials);
                isRegistered = true;
                System.out.println("Registration successful. You can now login.");
            } catch (AuthenticationServiceException e) {
                System.out.println("REGISTRATION ERROR: " + e.getMessage());
                System.out.println("Please attempt to register again.");
            }
        }
    }

    private void login() {
        System.out.println("Please log in");
        currentUser = null;
        while (currentUser == null) //will keep looping until user is logged in
        {
            UserCredentials credentials = collectUserCredentials();
            try {
                currentUser = authenticationService.login(credentials);
                accountService.AUTH_TOKEN = currentUser.getToken();
                userService.AUTH_TOKEN = currentUser.getToken();
                transferService.AUTH_TOKEN = currentUser.getToken();
            } catch (AuthenticationServiceException e) {
                System.out.println("LOGIN ERROR: " + e.getMessage());
                System.out.println("Please attempt to login again.");
            }
        }
    }

    private UserCredentials collectUserCredentials() {
        String username = console.getUserInput("Username");
        String password = console.getUserInput("Password");
        return new UserCredentials(username, password);
    }
}
