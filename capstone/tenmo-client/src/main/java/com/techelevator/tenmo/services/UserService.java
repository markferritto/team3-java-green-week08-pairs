package com.techelevator.tenmo.services;

import com.techelevator.tenmo.models.User;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class UserService {
    private final String BASE_SERVICE_URL;
    public String AUTH_TOKEN = "";
    private RestTemplate restTemplate = new RestTemplate();

    public UserService(String baseUrl) {
        this.BASE_SERVICE_URL = baseUrl;
    }

    public User[] retrieveAllUsers() {
        User[] users = null;

        try {
            users = restTemplate.exchange(BASE_SERVICE_URL + "users", HttpMethod.GET, makeAuthEntity(), User[].class).getBody();
        } catch (RestClientResponseException ex) {
            if (ex.getRawStatusCode() == 400) {
                return null;
            }
        }

        return users;
    }

    // MAKE AN AUTH ENTITY ONLY
    private HttpEntity makeAuthEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(AUTH_TOKEN);
        HttpEntity entity = new HttpEntity<>(headers);
        return entity;
    }
}
