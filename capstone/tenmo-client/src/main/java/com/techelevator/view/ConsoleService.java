package com.techelevator.view;


import com.techelevator.tenmo.models.Account;
import com.techelevator.tenmo.models.AuthenticatedUser;
import com.techelevator.tenmo.models.Transfer;
import com.techelevator.tenmo.models.User;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Scanner;

public class ConsoleService {

    private PrintWriter out;
    private Scanner in;

    public ConsoleService(InputStream input, OutputStream output) {
        this.out = new PrintWriter(output, true);
        this.in = new Scanner(input);
    }

    public Object getChoiceFromOptions(Object[] options) {
        Object choice = null;
        while (choice == null) {
            displayMenuOptions(options);
            choice = getChoiceFromUserInput(options);
        }
        out.println();
        return choice;
    }

    private Object getChoiceFromUserInput(Object[] options) {
        Object choice = null;
        String userInput = in.nextLine();
        try {
            int selectedOption = Integer.valueOf(userInput);
            if (selectedOption > 0 && selectedOption <= options.length) {
                choice = options[selectedOption - 1];
            }
        } catch (NumberFormatException e) {
            // eat the exception, an error message will be displayed below since choice will be null
        }
        if (choice == null) {
            out.println(System.lineSeparator() + "*** " + userInput + " is not a valid option ***" + System.lineSeparator());
        }
        return choice;
    }

    private void displayMenuOptions(Object[] options) {
        out.println();
        for (int i = 0; i < options.length; i++) {
            int optionNum = i + 1;
            out.println(optionNum + ") " + options[i]);
        }
        out.print(System.lineSeparator() + "Please choose an option >>> ");
        out.flush();
    }

    public String getUserInput(String prompt) {
        out.print(prompt + ": ");
        out.flush();
        return in.nextLine();
    }

    public Integer getUserInputInteger(String prompt) {
        Integer result = null;
        do {
            out.print(prompt + ": ");
            out.flush();
            String userInput = in.nextLine();
            try {
                result = Integer.parseInt(userInput);
            } catch (NumberFormatException e) {
                out.println(System.lineSeparator() + "*** " + userInput + " is not valid ***" + System.lineSeparator());
            }
        } while (result == null);
        return result;
    }

    public void printCurrentBalance(BigDecimal currentBalance) {
        System.out.println("Your current account balance is: $" + currentBalance);
    }

    public Transfer printListOfUsersToSendMoney(User[] users, String currentUser) {
        Transfer transfer = new Transfer();
        System.out.println("--------------------------------------------");
        System.out.println("Users");
        System.out.printf("%-10s %10s \n", "ID", "Name");
        System.out.println("--------------------------------------------");
        for (int i = 0; i < users.length; i++) {
            if (!users[i].getUsername().equals(currentUser)) {
                System.out.printf("%-10d %10s \n", users[i].getId(), users[i].getUsername());
            }
        }
        System.out.print("Enter ID of user you are sending to (0 to cancel): ");
        String userInput = in.nextLine();
        try {
            int selectedOption = Integer.parseInt(userInput);
            transfer.setAccountToId(selectedOption);

        } catch (NumberFormatException e) {

        }
        if (transfer.getAccountToId() == 0) {
            return transfer;
        }
        System.out.print("Enter amount: ");
        transfer.setTransferAmount(BigDecimal.valueOf(Double.parseDouble(in.nextLine())));
        transfer.setTransferTypeId(2);
        transfer.setTransferStatusId(2);

        return transfer;
    }

    public Transfer printListOfUsersToRequestMoney(User[] users, String currentUser) {
        Transfer transfer = new Transfer();
        System.out.println("--------------------------------------------");
        System.out.println("Users");
        System.out.printf("%-10s %10s \n", "ID", "Name");
        System.out.println("--------------------------------------------");
        for (int i = 0; i < users.length; i++) {
            if (!users[i].getUsername().equals(currentUser)) {
                System.out.printf("%-10d %10s \n", users[i].getId(), users[i].getUsername());
            }
        }
        System.out.print("Enter ID of user you are requesting from (0 to cancel): ");
        String userInput = in.nextLine();
        try {
            int selectedOption = Integer.parseInt(userInput);
            transfer.setAccountFromId(selectedOption);

        } catch (NumberFormatException e) {

        }
        if (transfer.getAccountFromId() == 0) {
            return transfer;
        } //else if (transfer.getAccountToId())
        System.out.print("Enter amount: ");
        transfer.setTransferAmount(BigDecimal.valueOf(Double.parseDouble(in.nextLine())));
        transfer.setTransferTypeId(1);
        transfer.setTransferStatusId(1);

        return transfer;
    }

    public int printListOfTransfers(Transfer[] transfers, int accountId) {

        System.out.println("\n*********** Listing Results ************\n");

        if (transfers.length == 0) {
            System.out.println("No Results Found!");
        }

        System.out.println("--------------------------------------------");
        System.out.println("Transfers");
        System.out.printf("%-10s %10s %10s \n", "ID", "From/To", "Amount");
        System.out.println("--------------------------------------------");
        for (int i = 0; i < transfers.length; i++) {
            if (transfers[i].getAccountFromId() == accountId) {
                System.out.printf("%-10d %10s %10s\n",
                        transfers[i].getTransferId(), "To: " + transfers[i].getAccountToUsername(), "$" + transfers[i].getTransferAmount());
            } else {
                System.out.printf("%-10d %10s %10s\n",
                        transfers[i].getTransferId(), "From: " + transfers[i].getAccountFromUsername(), "$" + transfers[i].getTransferAmount());
            }
        }
        System.out.println("Please enter transfer ID to view details (0 to cancel): ");

        return Integer.parseInt(in.nextLine());
    }

    public int printListOfPendingTransfers(Transfer[] transfers, int accountId) {

        System.out.println("\n*********** Listing Results ************\n");

        if (transfers.length == 0) {
            System.out.println("No Results Found!");
        }

        System.out.println("--------------------------------------------");
        System.out.println("Pending Transfers");
        System.out.printf("%-10s %10s %10s \n", "ID", "To", "Amount");
        System.out.println("--------------------------------------------");
        for (int i = 0; i < transfers.length; i++) {
            if (transfers[i].getAccountFromId() == accountId && transfers[i].getTransferStatusId() == 1) {
                System.out.printf("%-10d %10s %10s\n",
                        transfers[i].getTransferId(), transfers[i].getAccountToUsername(), "$" + transfers[i].getTransferAmount());
            }
        }
        System.out.println("Please enter transfer ID to approve/reject (0 to cancel): ");

        return Integer.parseInt(in.nextLine());
    }

    public int printPendingTransferOptions(Transfer transfer) {

        System.out.println("1: Approve\n" +
                "2: Reject\n" +
                "0: Don't approve or reject\n" +
                "---------\n" +
                "Please choose an option (number only please): ");

        return Integer.parseInt(in.nextLine());

    }

    public void printTransferDetails(Transfer transfer) {
        System.out.println("--------------------------------------------");
        System.out.println("Transfer Details");
        System.out.println("--------------------------------------------");
        System.out.println("Id: " + transfer.getTransferId());
        System.out.println("From: " + transfer.getAccountFromUsername());
        System.out.println("To: " + transfer.getAccountToUsername());
        if (transfer.getTransferTypeId() == 1) {
            System.out.println("Type: Request");
        } else {
            System.out.println("Type: Send");
        }
        if (transfer.getTransferStatusId() == 1) {
            System.out.println("Status: Pending");
        } else if (transfer.getTransferStatusId() == 2) {
            System.out.println("Status: Approved");
        } else {
            System.out.println("Status: Rejected");
        }
        System.out.println("Amount: $" + transfer.getTransferAmount());
    }


    public void printInsufficientFunds() {
        System.out.println("Insufficient funds.");
    }

    public void printNotValidOption() {
        System.out.println("Not a valid option, please try again.");
    }
}
